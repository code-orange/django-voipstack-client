import requests
import os


def voipstack_url():
    try:
        from django.conf import settings
    except ImportError:
        return os.getenv(
            "VOIPSTACK_API_URL", default="https://voipstack-api.example.com/"
        )

    return settings.VOIPSTACK_API_URL


def voipstack_default_credentials():
    try:
        from django.conf import settings
    except ImportError:
        username = os.getenv("VOIPSTACK_API_USER", default="user")
        password = os.getenv("VOIPSTACK_API_PASSWD", default="secret")
        return username, password

    username = settings.VOIPSTACK_API_USER
    password = settings.VOIPSTACK_API_PASSWD

    return username, password


def voipstack_head(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.head(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.headers


def voipstack_get_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.get(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    return response.json()


def voipstack_post_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.post(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def voipstack_patch_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def voipstack_put_data(
    endpoint: str, api_user: str, api_password: str, params=None, data=None
):
    if params is None:
        params = dict()

    if data is None:
        data = dict()

    response = requests.patch(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
        json=data,
    )

    return response.json()


def voipstack_delete_data(endpoint: str, api_user: str, api_password: str, params=None):
    if params is None:
        params = dict()

    response = requests.delete(
        f"{voipstack_url()}/{endpoint}",
        headers={
            "Authorization": "ApiKey {}:{}".format(
                api_user,
                api_password,
            ),
            "Content-Type": "application/json",
            "Accept": "application/json",
        },
        params=params,
    )

    if response.status_code == 200:
        return True

    return False


def voipstack_get_api_credentials_for_customer(mdat_id: int):
    username, password = voipstack_default_credentials()

    if username != mdat_id:
        response = voipstack_get_data(
            f"v1/apiauthcustomerapikey/{mdat_id}",
            username,
            password,
        )

        username = str(response["user"]["id"])
        password = response["key"]

    return username, password
